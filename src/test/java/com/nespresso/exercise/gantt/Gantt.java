package com.nespresso.exercise.gantt;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;


public class Gantt {

    private List<Task> tasks = new ArrayList<Task>();

    public void addTask(String name, int start, int end) {

        Task task = new Task();
        Timeline timeline = new Timeline();

        if (StringUtils.isNotEmpty(name)) {
            task.setName(name);
        }
        if (start >= 0) {
            timeline.setStart(start);
        }
        if (end > 0) {
            timeline.setDuration(end);
        }
        task.setTimeline(timeline);
        this.getTasks().add(task);
    }

    public String showPlanFor(String name) {
        Task task = getTaskForName(name);
        Task followingTask = getFollowingTaskForName(name);
        String taskName = task.getName();
        Timeline timeline = task.getTimeline();
        int nrOfRepeatsForLetter = timeline.getDuration();
        String firstLeterOfTaskName = String.valueOf(taskName.toUpperCase().charAt(0));
        int nrOfTasks = this.getTasks().size();

        //check if the current task is the single one in the list of tasks for the Gantt diagram
        if ((timeline.getStart() == 0) && (nrOfTasks == 1)) {
            createStringForSingleTask(firstLeterOfTaskName, nrOfRepeatsForLetter);
            // task is the first, but not the only one in the list
        } else if ((timeline.getStart() == 0) && (nrOfTasks > 1)) {
            createStringForFirstSmallerTask(firstLeterOfTaskName, nrOfRepeatsForLetter, timeline.getDuration() + 1);
        } else if ((timeline.getStart() != 0 && (nrOfTasks > 1))) {
            nrOfRepeatsForLetter = timeline.getDuration();
            int nrOfDotsForPreviousTask = getPreviousTaskForName(name).getTimeline().getDuration();
            boolean isLastTask = isLastTaskFromList(followingTask);
            return createStringForIntermediaryTask(firstLeterOfTaskName, nrOfRepeatsForLetter, nrOfDotsForPreviousTask, timeline.getDuration(), isLastTask);
        }

        return null;
    }

    public String createStringForSingleTask(String letter, int nrOfRepeats) {
        return StringUtils.repeat(letter, nrOfRepeats);
    }

    //returns a String for a task that is first in the list of tasks, but not the only one
    // which has a smaller duration than its next task in the list
    //smallerTask = task which has a smaller duration than its following task
    public String createStringForFirstSmallerTask(String letter, int nrOfRepeats, int dots) {
        return StringUtils.repeat(letter, nrOfRepeats) + StringUtils.repeat(".", dots);
    }

    //return a String for a intermediary task (containing dots before and after the letter) or for the last task
    public String createStringForIntermediaryTask(String letter, int nrOfRepeats, int dotsBefore, int dotsAfter, boolean isLastTask) {
        if (isLastTask) {
            return StringUtils.repeat(".", dotsBefore) + StringUtils.repeat(letter, nrOfRepeats);
        } else {
            return StringUtils.repeat(".", dotsBefore) + StringUtils.repeat(letter, nrOfRepeats) + StringUtils.repeat(".", dotsAfter);
        }

    }

    public Task getTaskForName(String name) {
        List<Task> tasks = this.getTasks();
        if (tasks != null && !tasks.isEmpty()) {
            for (Task task : tasks) {
                if (task.getName().equals(name)) {
                    return task;
                }
            }
        }
        return null;
    }

    public Task getFollowingTaskForName(String name) {
        List<Task> tasks = this.getTasks();
        for (int i = 0; i <= tasks.size(); i++) {
            if (tasks.get(i).getName().equals(name)) {
                return tasks.get(i++);
            }
        }
        return null;
    }

    public Task getPreviousTaskForName(String name) {
        List<Task> tasks = this.getTasks();
        for (int i = 0; i <= tasks.size(); i++) {
            if (tasks.get(i).getName().equals(name)) {
                return tasks.get(i--);
            }
        }
        return null;
    }

    public boolean isLastTaskFromList(Task task) {
        return this.getTasks().get(this.getTasks().size() - 1).equals(task);
    }

    public List<Task> getTasks() {
        return tasks;
    }
}
