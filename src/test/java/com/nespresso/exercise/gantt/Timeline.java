package com.nespresso.exercise.gantt;

/**
 * Created by evozon on 04/11/2016.
 */
public class Timeline {

    private int start;
    private int duration;

    public Timeline() {
    }

    public Timeline(int start, int duration) {
        this.start = start;
        this.duration = duration;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Timeline timeline = (Timeline) o;

        if (getStart() != timeline.getStart()) return false;
        return getDuration() == timeline.getDuration();

    }

    @Override
    public int hashCode() {
        int result = getStart();
        result = 31 * result + getDuration();
        return result;
    }
}
