package com.nespresso.exercise.gantt;

/**
 * Created by evozon on 04/11/2016.
 */
public class Task {

    private String name;
    private Timeline timeline;

    public Task() {
    }

    public Task(String name, Timeline timeline) {
        this.name = name;
        this.timeline = timeline;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timeline getTimeline() {
        return timeline;
    }

    public void setTimeline(Timeline timeline) {
        this.timeline = timeline;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Task task = (Task) o;

        if (!getName().equals(task.getName())) return false;
        return getTimeline().equals(task.getTimeline());

    }

    @Override
    public int hashCode() {
        int result = getName().hashCode();
        result = 31 * result + getTimeline().hashCode();
        return result;
    }
}
